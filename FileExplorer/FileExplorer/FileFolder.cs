using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Threading;
using FileExplorer.Annotations;

namespace FileExplorer
{
    public class FileFolder : INotifyPropertyChanged
    {
        #region variable
        public ObservableCollection<FileFolder> FilesFolders { get; set; }

        private static Dispatcher _dispatcher;

        private FileSystemWatcher _watcher;

        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }


        private string _path;

        public string Path
        {
            get { return _path; }
            set
            {
                _path = value;
                OnPropertyChanged();
            }
        }

        private DateTime? _modificationTime;

        public DateTime? ModificationTime
        {
            get { return _modificationTime; }
            set
            {
                _modificationTime = value;
                OnPropertyChanged();
            }
        }


        private long? _size;

        public long? Size
        {
            get { return _size; }
            set
            {
                _size = value;
                OnPropertyChanged();
            }
        }

        private int _level;

        public int Level
        {
            get { return _level; }
            set
            {
                _level = value;
                OnPropertyChanged();
            }
        }


        private bool _isLoaded;

        public bool IsLoaded
        {
            get { return _isLoaded; }
            set
            {
                _isLoaded = value;
                OnPropertyChanged();
            }
        }
        
        private bool _isAvailable;

        public bool IsAvailable
        {
            get { return _isAvailable; }
            set
            {
                _isAvailable = value;
                OnPropertyChanged();
            }
        }

        private FileFolderType _type;

        public FileFolderType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged();
            }
        }


        #endregion

        #region methods
        #endregion

        #region constructor

        static FileFolder()
        {
            _dispatcher = App.Current.Dispatcher;
        }

        //blank file
        private FileFolder() { }

        /// <summary>
        /// Folder constructor
        /// </summary>
        public FileFolder(string name, string path, int level, bool hasItems, bool isAvailable,  FileFolderType type = FileFolderType.Folder)
        {
            Name = name;
            Path = path;
            Level = level;
            Type = type;
            IsAvailable = isAvailable;
            FilesFolders = new ObservableCollection<FileFolder>();
            if (hasItems)
                FilesFolders.Add(new FileFolder());
        }

        /// <summary>
        /// File constructor
        /// </summary>
        public FileFolder(string name, long size, DateTime modificationTime, string path, int level)
        {
            Name = name;
            Size = size;
            Level = level;
            Type = FileFolderType.File;
            ModificationTime = modificationTime;
            Path = path;
        }
        #endregion

        #region watcher

        public void StartWatch()
        {
            if (_watcher == null) CreateWatcher();
            _watcher.EnableRaisingEvents = true;
        }

        public void StopWatch()
        {
            if (_watcher != null && _watcher.EnableRaisingEvents) _watcher.EnableRaisingEvents = false; 
        }

        private void CreateWatcher()
        {
            _watcher = new FileSystemWatcher(Path);
            _watcher.Changed += WatcherOnChanged;
            _watcher.Created += WatcherOnCreated;
            _watcher.Deleted += WatcherOnDeleted;
            _watcher.Renamed += WatcherOnRenamed;
        }

        private void WatcherOnRenamed(object sender, RenamedEventArgs renamedEventArgs)
        {
            var oldFile = FilesFolders.ToList().FirstOrDefault(x => x.Path.Equals(renamedEventArgs.OldFullPath));
            if (oldFile == null) return;
            oldFile.Path = renamedEventArgs.FullPath;
            oldFile.Name = renamedEventArgs.Name;
        }

        private void WatcherOnDeleted(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            var oldFile = FilesFolders.ToList().FirstOrDefault(x => x.Path == fileSystemEventArgs.FullPath);
            if (oldFile == null) return;
            _dispatcher.BeginInvoke(new Action(() => FilesFolders.Remove(oldFile)));
            //TODO: Shift-delete problem
        }

        private void WatcherOnCreated(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            var newFileFolder = FileFolderHelper.GetFileFolder(fileSystemEventArgs.FullPath, Level + 1);
            _dispatcher.BeginInvoke(new Action(() => FilesFolders.Add(newFileFolder)));
        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            var oldFile = FilesFolders.ToList().FirstOrDefault(x => x.Path == fileSystemEventArgs.FullPath);
            if (oldFile == null || oldFile.Type != FileFolderType.File) return;
            var info = new FileInfo(oldFile.Path);
            oldFile.Size = info.Length;
            oldFile.ModificationTime = info.LastWriteTime;
        }

        #endregion

        #region propertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}