using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace FileExplorer
{
    public class FolderDataSource : ObservableCollection<FileFolder>
    {
        public FolderDataSource()
        {
            Initialize();
        }

        private void Initialize()
        {
            foreach (var logicalDrive in DriveInfo.GetDrives())
            {
                if (logicalDrive.IsReady)
                {
                    var hasItems = Directory.GetFileSystemEntries(logicalDrive.Name).Any();
                    Add(new FileFolder(logicalDrive.Name, logicalDrive.Name, 1, hasItems, isAvailable: true, type: FileFolderType.HDD));
                }
            }
        }
    }
}