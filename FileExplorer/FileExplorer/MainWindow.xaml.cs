﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using FileExplorer.Annotations;

namespace FileExplorer
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region variable
        #endregion

        public MainWindow()
        {
            InitializeComponent();
        }


        #region methods
        private void LoadFoldersAndFiles(FileFolder fileFolder)
        {
            fileFolder.FilesFolders.Clear();
            //FileFolderHelper.LoadByThread(fileFolder);
            FileFolderHelper.LoadFilesFolders(fileFolder);
            fileFolder.IsLoaded = true;
        }

        #endregion

        #region events
        private void TreeViewItem_OnExpanded(object sender, RoutedEventArgs e)
        {
            if (!e.Handled)
            {
                e.Handled = true;
                var item = sender as TreeViewItem;
                var fileFolder = item.Header as FileFolder;
                
                if (!fileFolder.IsLoaded && fileFolder.Type != FileFolderType.File && fileFolder.IsAvailable)
                {
                    LoadFoldersAndFiles(fileFolder);
                }
            }
        }

        private void TreeViewItem_OnDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!e.Handled)
            {
                var fileFolder = (sender as TreeViewItem).Header as FileFolder;
                if (fileFolder.Type == FileFolderType.File)
                {
                    try
                    {
                        Process.Start(fileFolder.Path);
                    }
                    catch (Exception)
                    {

                    }
                    e.Handled = true;
                }

            }
        }
        #endregion

        #region propertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
