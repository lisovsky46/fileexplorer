namespace FileExplorer
{
    public enum FileFolderType
    {
        File,
        Folder,
        HDD
    }
}