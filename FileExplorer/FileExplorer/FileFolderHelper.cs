﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Windows;
using System.Windows.Documents;

namespace FileExplorer
{
    public static class FileFolderHelper
    {

        public static FileFolder GetFolder(DirectoryInfo directoryInfo, int level)
        {
            bool hasItems = true; //redundant
            bool isAvailable = true; //redundant
            return new FileFolder(directoryInfo.Name, directoryInfo.FullName, level, hasItems, isAvailable);
        }


        public static FileFolder GetFile(FileInfo fileInfo, int level)
        {
            return new FileFolder(fileInfo.Name, fileInfo.Length, fileInfo.LastWriteTime,
                           fileInfo.FullName, level);
        }

        /// <summary>
        /// If you don't know, what type of file it is
        /// </summary>
        public static FileFolder GetFileFolder(string path, int level)
        {
            var isFolder = Directory.Exists(path);
            if (isFolder)
                return GetFolder(new DirectoryInfo(path), level);
            return GetFile(new FileInfo(path), level);
        }

        #region FileSystemInfos solution

        public delegate FileSystemInfo[] GetFileSystemInfosDelegate(ref FileFolder parentFolder);

        public static FileSystemInfo[] GetFileSystemInfos(ref FileFolder parentFolder)
        {
            var folderInfo = new DirectoryInfo(parentFolder.Path);

            try
            {
                var childrenInfos = folderInfo.GetFileSystemInfos();
                parentFolder.StartWatch();
                return childrenInfos;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static void GetFilesFoldersFinish(IAsyncResult iResult)
        {
            var result = (AsyncResult)iResult;
            var asyncDelegate = (GetFileSystemInfosDelegate)result.AsyncDelegate;
            FileFolder parentFolder = null;
            var infos = asyncDelegate.EndInvoke(ref parentFolder, iResult);
            if (infos == null)
                return;

            int position = 0;
            int take = 500;
            int count = infos.Count();

            while (position < count)
            {
                foreach (var fileInfo in infos.Skip(position).Take(take))
                {
                    App.Current.Dispatcher.BeginInvoke(new Action(() => parentFolder.FilesFolders.Add(GetFileFolder(fileInfo.FullName, parentFolder.Level + 1))));
                }
                position += take;
                Thread.Sleep(500);
            }

        }


        public static void LoadFilesFolders(FileFolder parentFolder)
        {
            var deleg = new GetFileSystemInfosDelegate(GetFileSystemInfos);
            deleg.BeginInvoke(ref parentFolder, GetFilesFoldersFinish, null);
        }


        #endregion

        #region Thread solution

        public static void LoadByThread(FileFolder parentFolder)
        {
            var folderLoaderThread = new FolderLoaderThread(parentFolder);
            folderLoaderThread.LoadByThread();
        }

        public class FolderLoaderThread
        {
            public delegate void ThreadCallbackDelegate(FileFolder child, FileFolder parent);

            private FileFolder parentFolder;
            private ThreadCallbackDelegate callback;

            public FolderLoaderThread(FileFolder parentFolder)
            {
                this.parentFolder = parentFolder;
            }

            public void LoadByThread()
            {
                callback = LoadCallback;
                var thread = new Thread(Load);
                thread.Priority = ThreadPriority.Lowest;
                thread.IsBackground = true;
                thread.Start();
            }

            public void Load()
            {
                var directory = new DirectoryInfo(parentFolder.Path);
                IEnumerator<FileSystemInfo> enumerator;

                try
                {
                    enumerator = directory.EnumerateFileSystemInfos().GetEnumerator();
                    parentFolder.StartWatch();
                }
                catch (Exception)
                {
                    return;
                }

                while (enumerator.MoveNext())
                {
                    var newFileFolder = GetFileFolder(enumerator.Current.FullName, parentFolder.Level + 1);
                    callback(newFileFolder, parentFolder);
                    //Thread.Sleep(1); //more smooth
                }
            }

            public void LoadCallback(FileFolder child, FileFolder parent)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => parent.FilesFolders.Add(child)));
            }
        }

        #endregion

        /// <summary>
        /// Test a directory for create file access permissions
        /// </summary>
        /// <param name="DirectoryPath">Full path to directory </param>
        /// <param name="AccessRight">File System right tested</param>
        /// <returns>State [bool]</returns>
        public static bool DirectoryHasPermission(string DirectoryPath, FileSystemRights AccessRight)
        {
            if (string.IsNullOrEmpty(DirectoryPath))
                return false;

            try
            {
                AuthorizationRuleCollection rules = Directory.GetAccessControl(DirectoryPath).GetAccessRules(true, true, typeof(SecurityIdentifier));
                WindowsIdentity identity = WindowsIdentity.GetCurrent();

                foreach (FileSystemAccessRule rule in rules)
                {
                    if (identity.Groups.Contains(rule.IdentityReference))
                    {
                        if ((AccessRight & rule.FileSystemRights) == AccessRight)
                        {
                            if (rule.AccessControlType == AccessControlType.Allow)
                                return true;
                        }
                    }
                }
            }
            catch { }
            return false;
        }
    }
}
